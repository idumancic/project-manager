const path = require("path");

module.exports = {
  mode: "development",
  entry: "./src/app.ts",
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "dist"), // Webpack needs absolute path
    publicPath: "dist", // required for Webpack dev server where he loads build in memory and does not write to hard disk
  },
  devtool: "inline-source-map", // tells Webpack there will already be source maps available and he should use them and properly wire them
  module: {
    rules: [
      // tells Webpack how to handle TypeScript files
      {
        test: /\.ts$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: [".ts", ".js"], // tells Webpack to bundle all the files that have extension type of .ts or .js
  },
};
