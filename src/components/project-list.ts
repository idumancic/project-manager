import { Component } from "./base-component";
import { ProjectListItem } from './project-list-item';
import { Autobind } from "../decorators/autobind";

import { Project, ProjectStatus } from "../models/project";
import { DragTarget } from "../models/drag-drop";
import { projectState } from "../state/project";

export class ProjectList extends Component<HTMLDivElement, HTMLElement>
  implements DragTarget {
  private listElementId: string;
  assignedProjects: Project[];

  constructor(private type: "active" | "finished") {
    super("project-list", "app", false, `${type}-projects`);
    this.assignedProjects = [];
    this.listElementId = `${this.type}-projects-list`;

    this.configure();
    this.renderContent();
  }

  @Autobind
  dragOverHandler(event: DragEvent): void {
    if (event.dataTransfer && event.dataTransfer.types[0] === "text/plain") {
      event.preventDefault();
      const listEl = this.element.querySelector("ul")!;
      listEl.classList.add("droppable");
    }
  }

  @Autobind
  dropHandler(event: DragEvent): void {
    const projectId = event.dataTransfer!.getData("text/plain");
    projectState.moveProject(
      projectId,
      this.type === "active" ? ProjectStatus.Active : ProjectStatus.Finished
    );
  }

  @Autobind
  dragLeaveHandler(_: DragEvent): void {
    const listEl = this.element.querySelector("ul")!;
    listEl.classList.remove("droppable");
  }

  configure() {
    this.element.addEventListener("dragover", this.dragOverHandler);
    this.element.addEventListener("drop", this.dropHandler);
    this.element.addEventListener("dragleave", this.dragLeaveHandler);

    projectState.addListener((projects: Project[]) => {
      const relevantProjects = projects.filter((x) => {
        if (this.type === "active") {
          return x.status === ProjectStatus.Active;
        }

        return x.status === ProjectStatus.Finished;
      });

      this.assignedProjects = relevantProjects;
      this.renderProjects();
    });
  }

  renderContent() {
    this.element.querySelector("ul")!.id = this.listElementId;
    this.element.querySelector("h2")!.textContent =
      this.type.toUpperCase() + " PROJECTS";
  }

  private renderProjects() {
    const listEl = document.getElementById(
      this.listElementId
    )! as HTMLUListElement;
    listEl.innerHTML = "";

    for (const projectItem of this.assignedProjects) {
      new ProjectListItem(this.listElementId, projectItem);
    }
  }
}
