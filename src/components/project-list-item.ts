import { Component } from "./base-component";
import { Autobind } from "../decorators/autobind";

import { Project } from "../models/project";
import { Draggable } from "../models/drag-drop";

export class ProjectListItem
    extends Component<HTMLUListElement, HTMLLIElement>
    implements Draggable {
    private project: Project;

    get peoples() {
      if (this.project.people === 1) {
        return "1 person";
      }

      return `${this.project.people} persons`;
    }

    constructor(hostId: string, project: Project) {
      super("single-project", hostId, false, `project_${project.id}`);
      this.project = project;

      this.configure();
      this.renderContent();
    }

    @Autobind
    dragStartHandler(event: DragEvent): void {
      event.dataTransfer!.setData("text/plain", this.project.id.toString());
      event.dataTransfer!.effectAllowed = "move";
    }

    @Autobind
    dragEndHandler(_: DragEvent): void {
      console.log("DragEnd");
    }

    configure() {
      this.element.addEventListener("dragstart", this.dragStartHandler);
      this.element.addEventListener("dragend", this.dragEndHandler);
    }

    renderContent() {
      this.element.querySelector("h2")!.textContent = this.project.title;
      this.element.querySelector("h3")!.textContent =
        this.peoples + " assigned";
      this.element.querySelector("p")!.textContent = this.project.description;
    }
  }
